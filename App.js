import React, { Component } from 'react'
import Router from './src/Containers/Routes'

export default class App extends Component {
  render() {
    return (
      <Router />
    )
  }
}
