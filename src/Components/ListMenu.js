import React, { Component } from 'react'
import { View } from 'react-native'
import { List, Icon } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios'

class ListMenu extends Component {
    state = {
        firstName: ''
    }
    UNSAFE_componentWillMount() {
        const { profile } = this.props
        console.log("TOKEN:", this.props.profile[profile.length - 1].token);
        axios({
            method: 'get',
            url: 'https://zenon.onthewifi.com/ticGo/users',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(res => {
                const { data } = res
                const { user } = data
                console.log("RES:", data.user.firstName);
                this.setState({ email: data.user.email })
                this.setState({ lastName: data.user.lastName })
                this.setState({ firstName: data.user.firstName })
            }).catch(error => {
                console.log("ERROR:", error);
            })
    }
    Logout = () => {
        this.props.editProfile('', '', '', '', '')
        this.props.history.push('/intro')
    }
    goToLogin = () => {
        this.props.history.push('/login')
    }
    goToProfile = () => {
        this.props.history.push('/profile')
    }
    goToHistory = () => {
        this.props.history.push('/history')
    }
    render() {
        const { menu } = this.props
        const { profile } = this.props
        if (this.props.profile[profile.length - 1].token !== '') {
            return (
                <View data={menu}>
                    <List.Item style={{ backgroundColor: '#cc99ff' }}>
                        <Logo source={require('../MovieTick.png')} />
                    </List.Item>
                    <List.Item style={{ backgroundColor: '#00D084' }}>
                        <TextColor style={{ fontWeight: 'bold' }}>
                            <Icon name={'user'} size={'md'} color={'#000000'} /> : {this.state.firstName}
                        </TextColor>
                    </List.Item>
                    <List.Item onPress={this.goToProfile} style={{ backgroundColor: '#00D084' }}>
                        <TextColor>Profile</TextColor>
                    </List.Item>
                    <List.Item onPress={this.goToHistory} style={{ backgroundColor: '#00D084' }}>
                        <TextColor>History</TextColor>
                    </List.Item>
                    <List.Item onPress={this.Logout} style={{ backgroundColor: '#00D084' }}>
                        <TextColor>Logout</TextColor>
                    </List.Item>
                </View>)
        } else {
            return (
                <View data={menu}>
                    <List.Item style={{ backgroundColor: '#cc99ff' }}>
                        <Logo source={require('../MovieTick.png')} />
                    </List.Item>
                    <List.Item onPress={this.goToLogin} style={{ backgroundColor: '#00D084' }}>
                        <TextColor>Sign in</TextColor>
                    </List.Item>
                </View>
            )
        }
    }
}
const mapStateToProps = (state) => {
    return {
        menu: state.menu,
        profile: state.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        editProfile: (email, password, firstname, lastname, token) => {
            dispatch({
                type: 'EDIT_PROFILE',
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                token: token
            })
        },
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMenu)

const Logo = styled.Image`
    width: 180;
    height: 180;
`
const TextColor = styled.Text`
    color: #000;
    font-size: 18;
`