import React, { Component } from 'react'
import { View, Text, FlatList, Alert, TouchableOpacity, Image, ImageBackground, Modal } from 'react-native'
import { Card, WingBlank, WhiteSpace, Button, Tabs, Icon } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios'

var options = { weekday: 'short', month: 'short', day: 'numeric' }
var tab1 = Date.now()
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
tab2 = tab2.getTime()
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
tab3 = tab3.getTime()

class ListMovie extends Component {
    state = {
        movie: [],
        movies: [],
        modalVisible: false,
        movieShow1: [],
        movieShow2: [],
        movieShow3: [],
        tabs: [
            { title: `${new Date().toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
        ]
    }
    UNSAFE_componentWillMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => response.data)
            .then(data => {
                this.setState({ movies: data })
            })
    }
    getTimeMovie1 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab1 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow1: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }
    getTimeMovie2 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab2 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow2: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }
    getTimeMovie3 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab3 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow3: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }
    showMovie = (movie, movieID) => {
        this.setState({
            modalVisible: true,
            movie: movie,
        })
        this.getTimeMovie1(movieID)
        this.getTimeMovie2(movieID)
        this.getTimeMovie3(movieID)
    }

    closeMovie = () => {
        this.setState({
            modalVisible: false,
            movie: [],
            movieShow1: [],
            movieShow2: [],
            movieShow3: [],
        });
    }
    goToBooking = (item) => {
        const movie = item
        this.props.push('/booking', { movie: item })
    }
    render() {
        console.log(this.state.movies)
        return (
            <View>
                <FlatList style={{ padding: 5, alignContent: 'center' }}
                    data={this.state.movies} numColumns={2}
                    renderItem={({ item }) => {
                        console.log(item)
                        return <MovieCard>
                            <TouchableOpacity onPress={() => this.showMovie(item, item._id)}>
                                <Image style={{ width: 178, height: 264, borderRadius: 5, alignSelf: 'center' }}
                                    source={{ uri: item.image }} />
                                <MovieName>{item.name}</MovieName>
                            </TouchableOpacity>
                        </MovieCard>
                    }} />
                <Modal
                    animationType='fade'
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.')
                    }}>
                    <View style={{ flex: 1 }}>
                        <Top>
                            <SideMenu onPress={() => this.closeMovie()}>
                                <Icon name={'down'} color={'#000'} />
                            </SideMenu>
                            <Header>
                                <TextHeader>{this.state.movie.name}</TextHeader>
                            </Header>
                            <SideMenu>
                            </SideMenu>
                        </Top>
                        <Middle style={{
                            flex: 9, flexDirection: 'column'
                            , ImageBackground: `${uri = this.state.movie.image}`
                        }}>
                            <ImageBackground
                                blurRadius={5}
                                source={{ uri: this.state.movie.image }}
                                style={{
                                    flex: 1,
                                    flexDirection: 'column',
                                    height: '100%',
                                    width: '100%',
                                    justifyContent: 'center',
                                }}
                            >
                                <View style={{ padding: 5, flex: 1.4, alignItems: 'center', justifyContent: 'center' }}>
                                    <Image style={{ width: 200, height: 296 }}
                                        source={{ uri: this.state.movie.image }} />
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <WingBlank>
                                            <WhiteSpace size="sm" />
                                            <Text style={{
                                                fontSize: 18, fontWeight: 'bold'
                                                , color: '#ffffff', textShadowColor: '#000000'
                                            }}>
                                                Duration: {this.state.movie.duration} min.</Text>
                                            <WhiteSpace size="sm" />
                                        </WingBlank>
                                    </View>
                                </View>
                                <Tabs tabs={this.state.tabs} tabBarPosition='top'
                                    tabBarBackgroundColor='#00D084'
                                    tabBarActiveTextColor='#000000'
                                    tabBarInactiveTextColor='#ffffff'>
                                    <View style={{ flex: 8.6, alignItems: 'center' }} >
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow1}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}
                                                        style={{ backgroundColor: '#00D084' }}>
                                                        {this.addZero(new Date(item.startDateTime).getHours())}
                                                        :{this.addZero(new Date(item.startDateTime).getMinutes())}
                                                        {console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                    <View style={{ flex: 8.6, alignItems: 'center' }}>
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow2}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}
                                                        style={{ backgroundColor: '#00D084' }}>
                                                        {this.addZero(new Date(item.startDateTime).getHours())}
                                                        :{this.addZero(new Date(item.startDateTime).getMinutes())}
                                                        {console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                    <View style={{ flex: 8.6, alignItems: 'center' }}>
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow3}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}
                                                        style={{ backgroundColor: '#00D084' }}>
                                                        {this.addZero(new Date(item.startDateTime).getHours())}
                                                        :{this.addZero(new Date(item.startDateTime).getMinutes())}
                                                        {console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                </Tabs>
                            </ImageBackground>
                        </Middle>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default connect(state => state, { push })(ListMovie)

const MovieCard = styled(Card)`
    flex: 1;
    margin-bottom: 10;
    background-color: #26004d;
    border: 2px solid #26004d;
`
const MovieName = styled.Text`
    margin-top: 5;
    text-align: center;
    font-size: 18;
    font-weight: bold;
    color: #ffffff;
`
const Top = styled.View`
    flex: 1;
    flex-direction: row;
    background-color: #cc99ff;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const Middle = styled.View`
    flex: 9.25;
`