import React, { Component } from 'react';
import { Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Drawer, Icon, Button } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import ListMenu from '../Components/ListMenu'

var options = { year: 'numeric', month: 'long', day: 'numeric' }

class Booking extends Component {
    state = {
        movie: [],
        seats: [],
        sofa: [],
        premium: [],
        deluxe: [],
        checked: false,
        booking: []
    }
    UNSAFE_componentWillMount() {
        console.log('Prop movie:', this.props.location)
        this.setState({
            movie: this.props.location.state.movie
            , seats: this.props.location.state.movie.seats
        })
        this.setState({
            sofa: this.props.location.state.movie.seats[0]
            , premium: this.props.location.state.movie.seats[1]
            , deluxe: this.props.location.state.movie.seats[2]
        })
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }
    bookingChack = (row, col, type) => {
        console.log(row, col, type);
        this.setState({
            booking: [...this.state.booking, {
                type: type,
                row: row,
                column: col
            }]
        })
    }
    goToSubmit = () => {
        this.props.history.push('./bookingsubmit', { movie: this.state.movie, seats: this.state.booking })
    }
    goToMain = () => {
        this.props.history.push('./main')
    }

    render() {
        console.log('State Movie:', this.state.movie)
        console.log('State Seate:', this.state.seats)
        console.log('Sofa:', this.state.sofa)
        console.log('pre:', this.state.premium)
        console.log('delux:', this.state.deluxe)
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <View style={{ flex: 1 }}>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>BOOKING</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <View style={{
                        flex: 11, flexDirection: 'column',
                        backgroundColor: '#26004d',
                        justifyContent: 'center',
                    }}>
                        <ImageBackground
                            blurRadius={5}
                            source={{ uri: this.state.movie.movie.image }}
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                height: 200,
                                width: '100%',
                                justifyContent: 'center',
                            }}
                        >
                            <View style={{
                                flex: 1.5,
                                alignItems: 'center',
                                marginTop: 10,
                            }}>
                                <Image
                                    source={{ uri: this.state.movie.movie.image }}
                                    style={{ width: '100%', height: 180, resizeMode: 'contain' }}
                                />
                            </View>
                            <View style={{ flex: 2, marginTop: 70, alignItems: 'center' }}>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 18,
                                    color: 'white',
                                }}>
                                    {this.state.movie.movie.name}
                                </Text>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'white',
                                }}>
                                    <Icon name='clock-circle' size='xxs' color='white' /> {this.state.movie.movie.duration} min
                  </Text>
                                <Text style={{
                                    textAlign: 'center',
                                    fontSize: 15,
                                    color: 'white',
                                }}>
                                    {this.addZero(new Date(this.state.movie.startDateTime).getHours())}
                                    :{this.addZero(new Date(this.state.movie.startDateTime).getMinutes())}
                                    - {this.addZero(new Date(this.state.movie.endDateTime).getHours())}
                                    :{this.addZero(new Date(this.state.movie.endDateTime).getMinutes())}
                                </Text>
                            </View>
                        </ImageBackground>
                        <View style={{
                            flex: 1.85,
                            flexDirection: 'row',
                        }}>
                            <ScrollView>
                                <View style={{
                                    flex: 1,
                                    margin: 20,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                    <Image style={{ width: '100%', height: 50 }} source={require('../screen2.png')} />
                                    {
                                        this.state.seats.map((i, index) => {
                                            return Object.keys(i).map((rowsData, index) => {
                                                return (
                                                    <FlatList
                                                        data={i['row' + (index + 1)]}
                                                        numColumns={i.columns}
                                                        renderItem={(item) => {
                                                            const rowTes = [index + 1]
                                                            if (i.type === "SOFA") {
                                                                return (
                                                                    <TouchableOpacity disabled={item.item}
                                                                        onPress={() => { this.bookingChack(rowTes[0], item.index + 1, i.type) }}>
                                                                        <Image style={{
                                                                            width: 30,
                                                                            height: 10
                                                                        }} source={require('../seat3.png')} />
                                                                    </TouchableOpacity>
                                                                )
                                                            } else if (i.type === "PREMIUM") {
                                                                return (
                                                                    <TouchableOpacity disabled={item.item}
                                                                        onPress={() => { this.bookingChack(rowTes[0], item.index + 1, i.type) }}>
                                                                        <Image style={{
                                                                            width: 15,
                                                                            height: 15,
                                                                            margin: 1
                                                                        }} source={require('../seat2.png')} />
                                                                    </TouchableOpacity>
                                                                )
                                                            } else {
                                                                return (
                                                                    <TouchableOpacity disabled={item.item}
                                                                        onPress={() => { this.bookingChack(rowTes[0], item.index + 1, i.type) }}>
                                                                        <Image style={{
                                                                            width: 15,
                                                                            height: 15,
                                                                            margin: 1
                                                                        }} source={require('../seat1.png')} />
                                                                    </TouchableOpacity>
                                                                )
                                                            }
                                                        }} />
                                                )
                                            })
                                        }).reverse()
                                    }
                                </View>
                            </ScrollView>
                        </View>
                        <SubmitButton style={{ backgroundColor: '#00D084', alignSelf: 'center' }}
                            onPress={() => { this.goToSubmit() }}>Submit</SubmitButton>
                    </View>
                </Drawer>
            </View>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        push: location => {
            dispatch(push(location))
        }
    }
}
export default connect(null, { push })(Booking)

const Top = styled.View`
    flex: 1;
    flex-direction: row;
    background-color: #cc99ff;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const SubmitButton = styled(Button)`
    width: 100%;
    margin-top: 10;
    border: 2px solid #26004d;
`