import React, { Component } from 'react'
import { Drawer, Icon } from '@ant-design/react-native'
import styled from 'styled-components'
import ListMenu from '../Components/ListMenu'
import ListMovie from '../Components/ListMovie'

class Main extends Component {
    goToMain = () => {
        this.props.history.push('/main')
    }
    render() {
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>MOVIE TICK</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <ListMovie history={this.props.history} />
                    </Middle>
                </Drawer>
            </Background>
        )
    }
}

export default Main

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`