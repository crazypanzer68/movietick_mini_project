import React, { Component } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'

class Intro extends Component {
    componentDidMount() {
        this.props.addProfile('', '', '', '', '')
        this.timeoutHandle = setTimeout(() => {
            this.props.history.push('/main')
        }, 2000);
    }
    render() {
        return (
            <Background>
                <Center>
                    <Logo source={require('../MovieTick.png')} />
                </Center>
            </Background>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (email, password, firstname, lastname, token) => {
            dispatch({
                type: 'ADD_PROFILE',
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                token: token
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Intro)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Center = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;
`
const Logo = styled.Image`
    width: 260;
    height: 260;
`