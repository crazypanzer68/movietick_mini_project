import React, { Component } from 'react'
import { Button, Drawer, Icon, WhiteSpace } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

class Profile extends Component {
    state = {
        email: '',
        firstName: '',
        lastName: ''
    }
    UNSAFE_componentWillMount() {
        const { profile } = this.props
        console.log("TOKEN:", this.props.profile[profile.length - 1].token);
        axios({
            method: 'get',
            url: 'https://zenon.onthewifi.com/ticGo/users',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(res => {
                const { data } = res
                const { user } = data
                console.log("RES:", data.user.firstName);
                this.setState({ email: data.user.email })
                this.setState({ lastName: data.user.lastName })
                this.setState({ firstName: data.user.firstName })
            }).catch(error => {
                console.log("ERROR:", error);
            })
    }
    goToMain = () => {
        this.props.history.push('/main')
    }
    goToEditProfile = () => {
        this.props.push('/editprofile')
    }
    render() {
        const { profile } = this.props
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>PROFILE</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <Logo source={require('../MovieTick1.png')} resizeMode='stretch' />
                        <ProfileContent>
                            <TextProfile>E-mail : {this.state.email}</TextProfile>
                            <WhiteSpace />
                            <TextProfile>First name : {this.state.firstName}</TextProfile>
                            <WhiteSpace />
                            <TextProfile>Last name : {this.state.lastName}</TextProfile>
                            <WhiteSpace />
                        </ProfileContent>
                        <SubmitButton style={{ backgroundColor: '#00D084' }}
                            onPress={() => { this.goToEditProfile() }}>Edit Profile</SubmitButton>
                    </Middle>
                </Drawer>
            </Background >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        editProfile: (email, firstname, lastname) => {
            dispatch({
                type: 'EDIT_PROFILE',
                email: email,
                firstname: firstname,
                lastname: lastname
            })
        },
        push: location => {
            dispatch(push(location))
        },
        removeProfile: () => {
            dispatch({
                type: 'REMOVE_PROFILE',
                index: 0
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
    align-items: center;
`
const ProfileContent = styled.View`
`
const TextProfile = styled.Text`
    margin-top: 20;
    font-size: 20;
    color: #ffffff;
`
const SubmitButton = styled(Button)`
    width: 80%;
    margin-top: 10;
    border: 2px solid #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const Logo = styled.Image`
    margin-top: 10;
    margin-bottom: 20;
    border-radius: 5;
    width: 340;
    height: 145;
`