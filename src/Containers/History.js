import React, { Component } from 'react'
import { Image, Text, View, TouchableOpacity, FlatList } from 'react-native'
import { Drawer, Icon, Card, WingBlank } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

class History extends Component {
    state = {
        tickets: [],
        ticket: []
    }
    UNSAFE_componentWillMount() {
        const { profile } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
        })
            .then(res => {
                console.log('RES:', res.data);
                const dataTickets = res.data
                this.setState({ tickets: dataTickets })
                for (let index = 0; index < dataTickets.length; index++) {
                    console.log('Ticket INdex?:', dataTickets[index])
                    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${dataTickets[index].showtime}`)
                        .then(res => {
                            this.setState({ ticket: [...this.state.ticket, res.data] })
                            console.log('ticket:', this.state.ticket)
                            console.log('ticketS:', this.state.tickets)
                        })
                        .catch(error => {
                            console.log('ERROR_showtime:', error);
                        })
                }
            })
            .catch(error => {
                console.log('ERROR:', error);
            })
    }
    seatPrint = (seats) => {
        const seat = []
        for (let index = 0; index < seats.length; index++) {
            seat.push(<Text>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        return seat
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }
    goToMain = () => {
        this.props.history.push('/main')
    }

    render() {
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>HISTORY</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <FlatList style={{ padding: 5 }}
                            data={this.state.ticket}
                            renderItem={({ item, index }) =>
                                <Card style={{ marginBottom: 10 }}>
                                    <Card.Header
                                        title={item.movie.name}
                                        extra={item.movie.duration + ' min'}
                                    />
                                    <Card.Body>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View>
                                                <WingBlank>
                                                    <TouchableOpacity>
                                                        <Image style={{ width: 150, height: 222 }}
                                                            source={{ uri: item.movie.image }} />
                                                    </TouchableOpacity>
                                                </WingBlank>
                                            </View>
                                            <View>
                                                <Text>{new Date(this.state.tickets[index].createdDateTime).toLocaleDateString()}</Text>
                                                {this.seatPrint(this.state.tickets[index].seats)}
                                                <Text> Show time : {this.addZero(new Date(item.startDateTime).toLocaleDateString())} </Text>
                                                <Text> {this.addZero(new Date(item.startDateTime).getHours())}
                                                    : {this.addZero(new Date(item.startDateTime).getMinutes())}</Text>
                                            </View>
                                        </View>
                                    </Card.Body>
                                    <Card.Footer
                                        content="Soundtrack / Subtitle"
                                        extra={item.movie.soundtracks + '/' + item.movie.subtitles}
                                    />
                                </Card>
                            }
                        />
                    </Middle>
                </Drawer>
            </Background >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
export default connect(mapStateToProps)(History)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`