import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { Button, Drawer, Icon, InputItem, WhiteSpace } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

class EditProfile extends Component {
    state = {
        firstName: '',
        lastName: '',
        oldPassword: '',
        newPassword: '',
        confrimPassword: '',
        isShowChangePass: true
    }
    onClickChangeProfile = (firstName, lastName) => {
        const { profile } = this.props
        if (firstName === '') {
            alert("First Name can't be blank")
        } else if (lastName === '') {
            alert("Last Name can't be blank")
        } else {
            axios({
                method: 'put',
                url: 'https://zenon.onthewifi.com/ticGo/users',
                headers: { Authorization: `Bearer ${this.props.profile[profile.length - 1].token}` },
                data: {
                    firstName: firstName,
                    lastName: lastName
                }
            })
                .then(res => {
                    const { data } = res
                    const { user } = data
                    console.log("RES:", res);
                    this.props.history.push('/profile')
                }).catch(error => {
                    console.log("ERROR:", error);
                })
        }
    }
    onClickChangePassword = (oldPassword, newPassword, confrimPassword) => {
        const { profile } = this.props
        if (oldPassword === '') {
            alert("Old Password can't be blank")
        } else if (newPassword === '') {
            alert("New Passwordcan't be blank")
        } else if (confrimPassword === '') {
            alert("Confrim Passwordcan't be blank")
        } else if (newPassword !== confrimPassword) {
            alert('New Password does not match')
        }
        else {
            axios({
                method: 'put',
                url: 'https://zenon.onthewifi.com/ticGo/users/password',
                headers: { Authorization: `Bearer ${this.props.profile[profile.length - 1].token}` },
                data: {
                    oldPassword: oldPassword,
                    newPassword: newPassword
                }
            })
                .then(res => {
                    const { data } = res
                    const { user } = data
                    console.log("RES:", res);
                    this.props.history.push('/profile')
                }).catch(error => {
                    console.log("ERROR:", error);
                })
        }
    }
    goToMain = () => {
        this.props.history.push('/main')
    }
    render() {
        const { profile } = this.props
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>EDIT PROFLE</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <ScrollView style={{ flex: 1 }}>
                            <Logo source={require('../MovieTick1.png')} resizeMode='stretch' />
                            <LoginInput labelNumber={2} maxLength={254}
                                onChangeText={(value) => { this.setState({ firstName: value }) }}
                                placeholder='First name' placeholderTextColor='#ffffff'>
                                <Icon name={'robot'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={254}
                                onChangeText={(value) => { this.setState({ lastName: value }) }}
                                placeholder='Last name' placeholderTextColor='#ffffff'>
                                <Icon name={'robot'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <SubmitButton style={{ backgroundColor: '#00D084' }}
                                onPress={() => {
                                    this.onClickChangeProfile(this.state.firstName
                                        , this.state.lastName)
                                }}
                            >Save profile</SubmitButton>
                            <LoginInput labelNumber={2} maxLength={128} type='password'
                                onChangeText={(value) => { this.setState({ oldPassword: value }) }}
                                placeholder='Old password' placeholderTextColor='#ffffff'>
                                <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={128} type='password'
                                onChangeText={(value) => { this.setState({ newPassword: value }) }}
                                placeholder='New password' placeholderTextColor='#ffffff'>
                                <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={128} type='password'
                                onChangeText={(value) => { this.setState({ confrimPassword: value }) }}
                                placeholder='Confirm new password' placeholderTextColor='#ffffff'>
                                <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <SubmitButton style={{ backgroundColor: '#00D084' }}
                                onPress={() => {
                                    this.onClickChangePassword(this.state.oldPassword
                                        , this.state.newPassword
                                        , this.state.confrimPassword)
                                }}
                            >Save new password</SubmitButton>
                        </ScrollView>
                    </Middle>
                </Drawer>
            </Background >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        editProfile: (email, firstname, lastname) => {
            dispatch({
                type: 'EDIT_PROFILE',
                email: email,
                firstname: firstname,
                lastname: lastname
            })
        },
        push: location => {
            dispatch(push(location))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
    align-items: center;
`
const LoginInput = styled(InputItem)`
    flex: 8;
    margin-top: 20;
    font-size: 20;
    color: #ffffff;
`
const SubmitButton = styled(Button)`
    align-self: center;
    width: 80%;
    margin-top: 10;
    border: 2px solid #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const Logo = styled.Image`
    margin-top: 10;
    margin-bottom: 20;
    border-radius: 5;
    width: 340;
    height: 145;
`