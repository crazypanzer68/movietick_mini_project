import React, { Component } from 'react';
import { Text, View, FlatList } from 'react-native'
import { Button, Card, Drawer, Icon } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

var totalprice = 0

class BookingSubmit extends Component {
    state = {
        movie: [],
        seats: [],
        total: 0
    }
    UNSAFE_componentWillMount() {
        this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.seats })
    }
    goToMainWithSubmit = () => {
        const { profile } = this.props
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
            data: {
                showtimeId: this.state.movie._id,
                seats: this.state.seats
            }
        })
            .then(() => {
                this.props.history.push('./main')
                alert('Booking is Success')
            })
            .catch((error) => {
                console.log(error);
            })
    }
    goToMain = () => {
        this.props.history.push('./main')
    }
    price = (item) => {
        if (item === 'SOFA') {
            totalprice = totalprice + 500
            this.setState({ total: this.state.total + 500 })
            return 500
        } else if (item === 'PREMIUM') {
            totalprice = totalprice + 190
            this.setState({ total: this.state.total + 190 })
            return 190
        } else {
            totalprice = totalprice + 150
            this.setState({ total: this.state.total + 150 })
            return 150
        }
    }
    render() {
        const { profile } = this.props
        console.log('showTime Movie:', this.state.movie)
        console.log('seats:', this.state.seats)
        console.log('token:', this.props.profile[profile.length - 1].token)
        console.log('movie id :', this.state.movie._id)
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>BOOKING SUBMIT</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <FlatList style={{ padding: 5 }}
                            data={this.state.seats}
                            renderItem={({ item }) =>
                                <Card>
                                    <Card.Header
                                        title='Ticket'
                                    />
                                    <Card.Body>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View>
                                                <Text style={{ fontSize: 16 }}> SeatType:{item.type} </Text>
                                                <Text style={{ fontSize: 20 }}> R:{item.row} C:{item.column} </Text>
                                            </View>
                                            <View style={{ alignItems: 'center' }}>
                                                <Text style={{ fontSize: 25 }}> Price: {this.price(item.type)} THB </Text>
                                            </View>
                                        </View>
                                    </Card.Body>
                                </Card>
                            } />
                        <Text style={{ fontSize: 30, color: '#ffffff' }}>Total : {totalprice} THB</Text>
                        <SubmitButton style={{ backgroundColor: '#00D084' }}
                            onPress={() => { this.goToMainWithSubmit() }}>Confirm</SubmitButton>
                    </Middle>
                </Drawer>
            </Background>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
export default connect(mapStateToProps)(BookingSubmit)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const Middle = styled.View`
    flex: 9.25;
    align-items: center;
    background-color: #26004d;
`
const SubmitButton = styled(Button)`
    align-self: center;
    width: 100%;
    margin-top: 10;
    border: 2px solid #26004d;
`