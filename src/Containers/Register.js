import React, { Component } from 'react'
import { ScrollView } from 'react-native'
import { Button, InputItem, Drawer, Icon, WhiteSpace } from '@ant-design/react-native'
import styled from 'styled-components'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

class Register extends Component {
    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        passwordConfirm: ''
    }
    onEmailChange = (event) => {
        const email = event
        this.setState({ email })
    }
    onPasswordChange = (event) => {
        const password = event
        this.setState({ password })
    }
    saveProfile = (email, password, firstname, lastname, passwordConfirm) => {
        if (email === '') {
            alert("Email can't be blank")
        } else if (firstname === '') {
            alert("First Name can't be blank")
        }
        else if (lastname === '') {
            alert("Last Name can't be blank")
        }
        else if (password === '') {
            alert("Password can't be blank")
        }
        else if (passwordConfirm === '') {
            alert("Password Confirm can't be blank")
        }
        else if (password !== passwordConfirm) {
            alert('Password does not match')
        } else {
            axios({
                url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
                method: 'post',
                data: {
                    email: email,
                    password: password,
                    firstName: firstname,
                    lastName: lastname
                }
            }).then(res => {
                const { data } = res
                const { user } = data
                this.props.history.push('/login')
            }).catch(e => {
                if (e.response.data.errors.email === "duplicated email") {
                    alert('This email has already been used.')
                }
            })
        }
    }
    goToMain = () => {
        this.props.history.push('/main')
    }
    render() {
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>CREATE ACCOUNT</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <ScrollView style={{ flex: 1 }}>
                            <Logo source={require('../MovieTick1.png')} resizeMode='stretch' />
                            <LoginInput labelNumber={2} maxLength={254}
                                placeholder=' E-mail' placeholderTextColor='#ffffff'
                                onChangeText={(value) => { this.setState({ email: value }) }}>
                                <Icon name={'mail'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={254}
                                placeholder=' First name' placeholderTextColor='#ffffff'
                                onChangeText={(value) => { this.setState({ firstname: value }) }}>
                                <Icon name={'robot'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={254}
                                placeholder=' Last name' placeholderTextColor='#ffffff'
                                onChangeText={(value) => { this.setState({ lastname: value }) }}>
                                <Icon name={'robot'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={128} type='password'
                                placeholder=' Password' placeholderTextColor='#ffffff'
                                onChangeText={(value) => { this.setState({ password: value }) }}>
                                <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <LoginInput labelNumber={2} maxLength={128} type='password'
                                placeholder='Confirm password' placeholderTextColor='#ffffff'
                                onChangeText={(value) => { this.setState({ passwordConfirm: value }) }}>
                                <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                            </LoginInput>
                            <WhiteSpace />
                            <SubmitButton style={{ backgroundColor: '#00D084' }}
                                onPress={() => {
                                    this.saveProfile(this.state.email
                                        , this.state.password, this.state.firstname
                                        , this.state.lastname, this.state.passwordConfirm)
                                }}
                            >Create account</SubmitButton>
                        </ScrollView>
                    </Middle>
                </Drawer>
            </Background >
        )
    }
}

export default Register

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
    align-items: center;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const LoginInput = styled(InputItem)`
    margin-top: 20;
    font-size: 20;
    color: #ffffff;
`
const SubmitButton = styled(Button)`
    align-self: center;
    width: 80%;
    margin-top: 10;
    border: 2px solid #26004d;
`
const Logo = styled.Image`
    margin-top: 10;
    margin-bottom: 20;
    border-radius: 5;
    width: 340;
    height: 145;
`