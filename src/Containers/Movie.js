import React, { Component } from 'react'
import { Text, View, ScrollView, Image, FlatList } from 'react-native'
import { Drawer, Button, Icon, WhiteSpace, WingBlank, Tabs } from '@ant-design/react-native'
import NavigationBar from 'react-native-navbar'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import styled from 'styled-components'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

var options = { weekday: 'short', month: 'short', day: 'numeric' }
var tab1 = Date.now()
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
tab2 = tab2.getTime()
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
tab3 = tab3.getTime()

class Movie extends Component {
    state = {
        movie: [],
        movieShow1: [],
        movieShow2: [],
        movieShow3: [],
        tabs: [
            { title: `${new Date().toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
            { title: `${new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
        ]
    }
    getTimeMovie1 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab1 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow1: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }

    getTimeMovie2 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab2 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow2: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }

    getTimeMovie3 = (id) => {
        console.log(new Date());
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
            params: { date: tab3 }
        })
            .then(response => response.data)
            .then(data => {
                this.setState({ movieShow3: data })
                // console.log('movie show:',this.state.movieShow);
            })
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }
    openMovie = (movie, movieID) => {
        this.setState({
            movie: movie
        })
        this.getTimeMovie1(movieID)
        this.getTimeMovie2(movieID)
        this.getTimeMovie3(movieID)
    }
    goToMain = () => {
        this.props.history.push('/main')
    }
    goToBooking = (item) => {
        const { push } = this.props
        const movie = this.props.location.state.item
        push('/booking', {
            item: item, movie: movie
        })
    }
    render() {
        const sidebar = (
            <ListMenu history={this.props.history} />
        )

        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>MOVIE TICK</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <View style={{ flex: 1 }}>
                            <View style={{ flex: 1 }} >
                                <NavigationBar title={{ title: `${this.state.movie.name}` }}
                                    leftButton={{
                                        title: 'Back',
                                        handler: () => this.closeMovie()
                                    }}
                                    statusBar={{ showAnimation: 'slide', tintColor: 'black' }} />
                            </View>
                            <View style={{ flex: 10, flexDirection: 'column' }}>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <Image style={{ width: 200, height: 296 }}
                                        source={{ uri: this.state.movie.image }} />
                                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                        <WingBlank>
                                            <WhiteSpace size="sm" />
                                            <Text>Duration: {this.state.movie.duration} min.</Text>
                                            <WhiteSpace size="sm" />
                                        </WingBlank>
                                    </View>
                                </View>
                                <Tabs tabs={this.state.tabs} tabBarPosition="top">
                                    <View style={{ flex: 5, alignItems: 'center' }}>
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow1}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}{console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                    <View style={{ flex: 5, alignItems: 'center' }}>
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow2}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}{console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                    <View style={{ flex: 5, alignItems: 'center' }}>
                                        <WhiteSpace />
                                        <FlatList
                                            data={this.state.movieShow3}
                                            numColumns={4}
                                            renderItem={({ item }) =>
                                                <WingBlank size="sm">
                                                    <Button onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}{console.log(new Date(item.startDateTime))}
                                                    </Button>
                                                    <WhiteSpace />
                                                </WingBlank>
                                            }
                                        />
                                    </View>
                                </Tabs>
                            </View>
                            <View>
                            </View>
                        </View>
                    </Middle>
                </Drawer>
            </Background>
        )
    }
}

export default connect(state => state, { push })(Movie)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`