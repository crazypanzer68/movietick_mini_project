import React, { Component } from 'react'
import { Button, Drawer, Icon, InputItem, WhiteSpace } from '@ant-design/react-native'
import styled from 'styled-components'
import { connect } from 'react-redux'
import ListMenu from '../Components/ListMenu'
import axios from 'axios'

class Login extends Component {
    state = {
        email: '',
        password: '',
    }
    onValueChange = (field, value) => {
        this.setState({ [field]: value })
    }
    goToMainWithLogin = (email, password) => {
        console.log('Email:' + email);
        console.log('Password:' + password);
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
            method: 'post',
            data: {
                email: email,
                password: password,
            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.addProfile(data.user.email, data.user.password
                , data.user.firstName, data.user.lastName, data.user.token)
            this.props.history.push('/main')
        }).catch(e => {
            if (e.response.data.errors.email === "Email can't be blank") {
                alert(e.response.data.errors.email)
            } else if (e.response.data.errors.password === "Password can't be blank") {
                alert(e.response.data.errors.password)
            }
            console.log("error " + e.response.data.errors.email)
            console.log("error " + e.response.data.errors.password)
        })
    }
    goToMain = () => {
        this.props.history.push('/main')
    }
    goToRegister = () => {
        this.props.history.push('/register')
    }
    render() {
        const { profile } = this.props
        const sidebar = (
            <ListMenu history={this.props.history} />
        )
        return (
            <Background>
                <Drawer
                    sidebar={sidebar}
                    position='left'
                    open={false}
                    drawerRef={el => (this.drawer = el)}
                    onOpenChange={this.onOpenChange}
                    drawerBackgroundColor='#26004d'
                    drawerWidth={210}
                >
                    <Top>
                        <SideMenu onPress={() => this.drawer && this.drawer.openDrawer()}>
                            <Icon name={'menu'} color={'#000'} />
                        </SideMenu>
                        <Header>
                            <TextHeader>SIGN IN</TextHeader>
                        </Header>
                        <SideMenu onPress={this.goToMain}>
                            <Icon name={'home'} color={'#000'} />
                        </SideMenu>
                    </Top>
                    <Middle>
                        <Logo source={require('../MovieTick1.png')} resizeMode='stretch' />
                        <LoginInput labelNumber={2} maxLength={254}
                            onChangeText={value => { this.onValueChange("email", value) }}
                            placeholder=' E-mail' placeholderTextColor='#ffffff'>
                            <Icon name={'mail'} size={'md'} color={'#ffffff'} />
                        </LoginInput>
                        <WhiteSpace />
                        <LoginInput labelNumber={2} maxLength={128} type='password'
                            onChangeText={value => { this.onValueChange("password", value) }}
                            placeholder=' Password' placeholderTextColor='#ffffff'>
                            <Icon name={'lock'} size={'md'} color={'#ffffff'} />
                        </LoginInput>
                        <WhiteSpace />
                        <SubmitButton style={{ backgroundColor: '#00D084' }} onPress={() => {
                            this.goToMainWithLogin(this.state.email, this.state.password)
                        }}>Login</SubmitButton>
                        <SubmitButton style={{ backgroundColor: '#e6ffe6' }} onPress={this.goToRegister}>
                            Not registered? Create an account</SubmitButton>
                    </Middle>
                </Drawer>
            </Background >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addProfile: (email, password, firstname, lastname, token) => {
            dispatch({
                type: 'ADD_PROFILE',
                email: email,
                password: password,
                firstname: firstname,
                lastname: lastname,
                token: token
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

const Background = styled.View`
    flex: 1;
    background-color: #cc99ff;
`
const Top = styled.View`
    flex: 0.75;
    flex-direction: row;
    margin-top: 5;
    margin-bottom: 5;
`
const Header = styled.View`
    justify-content: center;
    align-items: center;
    width: 60%;
`
const TextHeader = styled.Text`
    font-size: 24;
    font-weight: bold;
    color: #000;
`
const Middle = styled.View`
    flex: 9.25;
    background-color: #26004d;
    align-items: center;
`
const LoginInput = styled(InputItem)`
    margin-top: 20;
    font-size: 20;
    color: #ffffff;
`
const SubmitButton = styled(Button)`
    width: 80%;
    margin-top: 10;
    border: 2px solid #26004d;
`
const SideMenu = styled.TouchableOpacity`
    flex: 1;
    justify-content: center;
    align-items: center;
    width: 20%;
`
const Logo = styled.Image`
    margin-top: 10;
    margin-bottom: 20;
    border-radius: 5;
    width: 340;
    height: 145;
`