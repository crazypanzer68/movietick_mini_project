import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { store, history } from '../Reducers/Store'
import Intro from './Intro'
import Main from './Main'
import Login from './Login'
import Profile from './Profile'
import EditProfile from './EditProfile'
import Register from './Register'
import Movie from './Movie'
import Booking from './Booking'
import BookinSubmit from './BookingSubmit'
import History from './History'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path='/' component={Intro} />
                        <Route exact path='/main' component={Main} />
                        <Route exact path='/login' component={Login} />
                        <Route exact path='/profile' component={Profile} />
                        <Route exact path='/editprofile' component={EditProfile} />
                        <Route exact path='/register' component={Register} />
                        <Route exact path='/movie' component={Movie} />
                        <Route exact path='/booking' component={Booking} />
                        <Route exact path='/bookingsubmit' component={BookinSubmit} />
                        <Route exact path='/history' component={History} />
                        <Redirect to='/' />
                    </Switch>
                </ConnectedRouter>
            </Provider >
        )
    }
}

export default Router