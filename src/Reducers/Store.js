import { createMemoryHistory } from 'history'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import profileReducer from '../Reducers/UserReducer'

const reducers = (history) => combineReducers({
    profile: profileReducer,
    router: connectRouter(history)
})

export const history = createMemoryHistory()

export const store = createStore(
    reducers(history),
    compose(
        applyMiddleware(
            routerMiddleware(history)
        )
    )
)